﻿var assert = require('chai').assert;

var Wrapper = require('./Wrapper');
var WrapperController = new Wrapper();

describe('Classe Wrapper', function () {

    describe('Metodo: isDatabase', function () {
         /*
            * Exemplos para teste:
            *
            *   - SqlServer: jdbc:sqlserver://localhost;integratedSecurity=true
            *   - Postgres: postgres://localhost:5432/express_tdd_testing
            *   - Oracle: jdbc:oracle:thin:@myhost:1521:orcl
            *
         */
        
        it('A URL deve ser uma conexão de banco', function () {
            var urlRequest = 'oracle:thin:@myhost:1521:orcl';
            //var urlRequest = 'testando_erro';
            WrapperController.isDatabase(urlRequest, function (is) {
                assert(is, 'Não é um database connection');
            });
        })
    });
    
    describe('Metodo: isFile', function () {
         /*
            * Exemplos para teste:
            *
            *   - fileURL: file:///User/Danny/Desktop/javascriptWork/testing.txt
            *
         */

        it('A requisição deve ser um arquivo', function () {
            var urlRequest = "file:///User/Danny/Desktop/javascriptWork/testing.txt";
            //var urlRequest = "testando_erro";
            WrapperController.isFile(urlRequest, function (is) {
                assert(is, 'Não é um arquivo: ');
            });
        })
    });

    describe('Metodo: isService', function () {
        /*
            * Exemplos para teste:
            *
            *   - WMS: https://mapas.ibge.gov.br/interativos/servicos/wms-do-arcgis.html
            *   - WMS: https://demo.boundlessgeo.com/geoserver/topp/wms?service=WMS&version=1.1.0&request=GetMap&layers=topp:tasmania_water_bodies
            *   - ArcGIS: http://tecgeo.maps.arcgis.com/home/item.html?id=53f65088621545f585cacaf77dcfc7df
            *   - GeoServer: https://demo.boundlessgeo.com/geoserver/topp/wms?service=WMS&version=1.1.0&request=GetMap&layers=topp:tasmania_water_bodies
            *   - Uma camada: http://sigtes.itep.br/i3geo/ogc.php?tema=UnibaseLotesA
            *   - Todas as camadas: http://sigtes.itep.br/i3geo/ogc.php
            *
        */
        it('A URL deve ser um webservice', function () {
            var urlRequest = 'https://mapas.ibge.gov.br/interativos/servicos/wms-do-arcgis.html';
            //var urlRequest = 'testando_erro';
            WrapperController.isService(urlRequest, function (is, flag) {
                assert(is, 'Não é um webservice flag: '+flag);
            });
        })
    });
});
