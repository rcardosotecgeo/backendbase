﻿'use strict';
var express = require('express');
var wmsclient = require("wms-client");
var wfs = require('wfs-client');
var StringFile = require('../util/stringfile');

var Database = require('../models/database/Database');
var File = require('../models/file/File'); 
var Service = require('../models/service/Service');

class Wrapper {

    despachar(req, res, next) {
        /* Despacha para models específico */
        verificarTipo(req, function (tipo) {
            if (tipo === 'database') {
                var databaseModel = new Database();
                //TODO: Analisar e retornar sobre o database
            } else if (tipo === 'file') {
                var fileModel = new File();
                //TODO: Analisar e retornar sobre o file
            } else if (tipo === 'service') {
                var serviceModel = new Service();
                //TODO: Analisar e retornar sobre o service
            } else if (tipo == null || tipo == 'undefined') {
                //TODO: retornar alerta de tipo não identificado
            }
        });
    }

    verificarTipo(req, callback) {
        /* Verificar tipo de requisição */
        var urlRequest = req.body.requisicao;

        isDatabase(urlRequest, function (is) {
            if (is) {
                callback('database');
            }
        });

        isFile(urlRequest, function (is) {
            if (is) {
                callback('file');
            }
        });

        isService(urlRequest, function (is) {
            if (is) {
                callback('service');
            }
        });
    }

    isDatabase(dataSource, callback) {
        var dbDrivers = JSON.stringify(StringFile.database);
     
        if (dataSource.search(":") != -1) {
            var URLconn = dataSource.split(":");
            var driver = URLconn[0];
            if (driver.startsWith("jdbc")) {
                callback(true);
            } else {
                if (dbDrivers.search(driver) != -1) {
                    callback(true);
                } else {
                    callback(false);
                }      
            }
        } else {
            callback(false);
        }     
    }

    isFile(dataSource, callback) {  
        if (dataSource.search("file:") != -1) {
            callback(true);
        } else {
            callback(false);
        }  
    }

    isService(dataSource, callback) {    
        if (dataSource.search("http:") != -1 || dataSource.search("https:") != -1) {
            isWMS(function (is, result) {
                //
            });

            isWFS(function (is, result) {
                //
            });



        } else {
            callback(false, "Nao tem https");
        }   
    }

    isWMS(url, callback) {
        var wms = wmsclient(dataSource);
        wms.capabilities(function (err, capabilities) {
            if (!err) {
                callback(true, capabilities.service.title);
            } else {
                callback(false, err);
            }
        });
    }

    isWFS(url, callback) {
        var client = wfs('http://your-wfs-server.tld/wfs', options);
        client.capabilities()
            .done(function (capabilities) {
                callback(true, capabilities.service);
            })
            .fail(function (err) {
                callback(false, err);
            });
    }
}

module.exports = Wrapper;