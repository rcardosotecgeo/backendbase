var express = require('express');
var router = express.Router();

var UserCtrl = require('../controllers/user/userCtrl');
var userCtrl = new UserCtrl();

/* Toda as rotas do sistema */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/home', function(req, res, next) {
  res.render('home');
});

router.post('/user/login', userCtrl.login);




module.exports = router;
