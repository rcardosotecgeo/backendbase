'use strict';
const Sequelize = require('sequelize');

module.exports = class Connection {

	start() {
		const sequelize = new Sequelize('database', 'username', 'password', {
		  host: 'localhost',
		  dialect: 'postgres',

		  pool: {
		    max: 5,
		    min: 0,
		    acquire: 30000,
		    idle: 10000
		  },
		});

		const sequelize = new Sequelize('postgres://user:pass@example.com:5432/dbname');
	}
	
}